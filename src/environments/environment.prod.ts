export const environment = {
  production: true,
  baseUrl: 'https://course-angular-expert.herokuapp.com/api'
};
