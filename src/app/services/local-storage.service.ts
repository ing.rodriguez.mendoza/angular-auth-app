import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() { }

  setLocalStorage(keyValue: string,value: string){
    localStorage.setItem(keyValue, value);
  }

  getLocalStorage(keyValue: string){
    // localStorage.getItem(`'${keyValue}'`) || '';
    return localStorage.getItem(keyValue);
  }

  clearLocalStorage(){
    localStorage.clear();
  }


}
