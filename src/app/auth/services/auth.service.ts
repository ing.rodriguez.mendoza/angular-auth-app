import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { of, Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { environment } from '../../../environments/environment';
import { AuthResponse, Usuario } from '../interfaces/interfaces';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private baseUrl = environment.baseUrl;
  private _usuario!: Usuario;

  get usuario(){
    // ...para evitar que en algun lugar yo manipule el usuario
    return { ...this._usuario }
  }


  constructor(
    private http: HttpClient,
    private localStorageService:LocalStorageService) {
  }

  login(email: string, password: string){
    const url = `${this.baseUrl}/auth`;
    const body = { email, password };
    //retornado un observable
    return this.http.post<AuthResponse>(url, body)
    .pipe(
      tap( resp => {
//        console.log( resp );
        if (resp.ok) {
          this.localStorageService.setLocalStorage('token', resp.token!);
          console.log( 'this._usuario ', this._usuario  )
        }
      }),
      map( (rep) => rep.ok  ),
      catchError( err => {
          this.localStorageService.clearLocalStorage();
          return of(err.error.msg);
      } ) );
//    catchError( err => of(err) ) );
  }

  validarToken(): Observable<boolean> {

    const url = `${this.baseUrl}/auth/renew`;
    const headers =
     new HttpHeaders()
     .set('x-token',
          this.localStorageService.getLocalStorage('token')!);

    return this.http.get<AuthResponse>(url, { headers })
    .pipe(
      map( resp=>{
        this.localStorageService.setLocalStorage('token', resp.token!);
        this._usuario = {
          name: resp.name!,
          uid: resp.uid!,
          email: resp.email!
        }
        console.log( 'this._usuario ', this._usuario  )

        return resp.ok
      }),
      catchError( err => {
        this.localStorageService.clearLocalStorage();
        return of(false)
      } )
    );

  }

  logout(){
    this.localStorageService.clearLocalStorage();
  }

  registro(usuario:Usuario){
    const url  = `${this.baseUrl}/auth/new`;

    return this.http.post<AuthResponse>(url, usuario)
    .pipe(
      // tap( resp => {
      tap( ({ok , token}) => {
        // console.log( 'resp registro', resp );
        if (ok) {
          this.localStorageService.setLocalStorage('token', token!);
        }
      }),
      map( (rep) => rep.ok  ),
      catchError( err => {
        this.localStorageService.clearLocalStorage();
        return of(err.error.msg);
      } ) );
  }

}
