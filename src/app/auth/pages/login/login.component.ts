import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: [
  ]
})
export class LoginComponent implements OnInit {

  miFormulario!: FormGroup;


  constructor(
    private fb: FormBuilder,
    private router:Router,
    private authService:AuthService) { }

  ngOnInit(): void {

    this.miFormulario = this.fb.group({
      email:    ['test1@test.com', [ Validators.required, Validators.email ]],
      password: ['123456', [ Validators.required, Validators.minLength(6) ]]
    })
  }

  login(){

    // this.authService.validarToken()
    // .subscribe( console.log );

    console.log( this.miFormulario.value );
    console.log( this.miFormulario.valid );

    const { email, password } = this.miFormulario.value;

    this.authService
    .login( email, password )
    .subscribe( resp=> {

      console.log( 'resp', resp );

      if (resp === true) {
        this.router.navigateByUrl('/dashboard');
      }
      else {
        // console.log( 'resp' );
        // console.log( resp );
        // TODO: Mostrar mensaje de error
        Swal.fire('Error', resp , 'error')
      }
    });
  }

  campoNoValido(campo: string){
    return this.miFormulario.get(campo)?.errors
    && this.miFormulario.get(campo)?.touched
  }

}
