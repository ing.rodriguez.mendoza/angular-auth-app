import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { Usuario } from '../../interfaces/interfaces';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styles: [
  ]
})
export class RegisterComponent implements OnInit {

  usuario!: Usuario;

  constructor(
    private fb:FormBuilder,
    private router:Router,
    private authService:AuthService) { }

  miFormulario: FormGroup = this.fb.group({
    nombre: ['Test 4', [Validators.required, Validators.minLength(3)]],
    email: ['test4@test.com', [Validators.required, Validators.email]],
    password: ['123456', [Validators.required, Validators.minLength(6)]],
  })


  ngOnInit(): void {
  }

  campoNoValido(campo: string){
    return this.miFormulario.get(campo)?.errors
    && this.miFormulario.get(campo)?.touched
  }

  register(){
    console.log( this.miFormulario.value );
    console.log( this.miFormulario.valid );

    const { nombre, email, password } = this.miFormulario.value;

    // console.log( 'nombre, email, password', nombre, email, password )

    // this.usuario.name = nombre;
    this.usuario = {
      name: nombre,
      email,
      password
    }

    this.authService.registro( this.usuario ).subscribe( resp=> {

      console.log( 'resp in reg', resp );

      if (resp === true) {
        this.router.navigateByUrl('/dashboard');
      }
      else {
        // console.log( 'resp' );
        // console.log( resp );
        // TODO: Mostrar mensaje de error
        Swal.fire('Error', resp , 'error')
      }
    });;

    // this.router.navigateByUrl('/protected');
  }

}
